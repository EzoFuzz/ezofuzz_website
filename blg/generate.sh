#!/bin/bash

# Get script directory
sc_dir=$(dirname $0)

# Delete previous gen
rm "$sc_dir/blg_dst/.files"

# Execute SSG5
$sc_dir/_custom_ssg5 "$sc_dir/blg_src/" "$sc_dir/blg_dst/" "EzoFuzz: Blog" "https://ezofuzz.me"
