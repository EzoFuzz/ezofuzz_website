---
title: Hello World! - Stay tuned.
author: Luca Hölzel
date: 25.02.2021
description: Hello World, indeed! This is the first post on this blog, and a test for the customized SSG5 blog and feed generator. Many more posts and other shananigans will follow shortly. Or read more about this website setup in this article.
image: img/brand/MainLogo_PFP.png
---

# Ezo Fuzz: A long time coming.

For years I have been wanting to set up a website for myself. As is the case with many of my projects, I ended up putting it off and when I didn't, I lost myself in details, trying to create a truly minimal setup, which relies on as little outside resources and foreign code as possible. Such is the life of a software minimalist.

Now as I am nearing my bachelor semester, getting ready to begin my research and setting out to work in the technology and video game industry, I could not let up with the need to actually have my own land in cyberspace. This of course sounds cheesy in the astronomical year of 2021. However, the grasp of social media, and the ruthless companies running them, has extended further into our private lives, professional occupations and even the political and ideological marketplace, as anyone would have guessed 10 or 20 years ago.

In the same manner that many Gen-Z youth seem to find themselves agreeing with more conservative talking points, as compared to millenials before them, many younger people are now putting forth their intentions of creating their own website, instead of just creating a feed on the hand full of social media platforms at our disposal.

This blog serves for me to express my findings in the realms of game development, open source software and others.
To keep up to date, simply follow the feed:
[Subscribe to my RSS feed!](https://ezofuzz.me/feed.xml)


# Website Setup.

This website is written in completely custom html and css. No creation site or fancy (aka slow) framework used. It is hosted via gitlab pages, to ensure fast access speeds and easy updating via git on the command line.

To generate these blog entries, I write them in markdown within a specific folder structure. These markdown files are then converted into correctly formatted html files, before generating a [blog feed page](https://ezofuzz.me/chop.html) .

The conversion process is achieved using a modified version of [SSG5](https://www.romanzolotarev.com/ssg.html).
This surprisingly efficient yet simple tool was written by Roman Zolotarev in 2018. Instead of complicating and abstracing this process, this tool is literally just a ~250 loc bash script, which does everything using basic unix shell commands. This means, that changing or adding functionality is as simple as just writing a few lines of shell. 

The main addition, which I included in my version, is the *(somewhat hacky)* generation of an RSS feed, which chronologically lists all blog articles, as well as copying the generated HTML and RSS into template files. This is done by simply inserting a chain of printf instructions below the page rendering code and defining a few directories in the setup function:

```
 289   │     # Generate RSS Item
 290   │     printf "\
 291   │ <item>\n\
 292   │ <title>$TITLE</title>\n\
 293   │ <link>$5/${f%\.md}.html</link>\n\
 294   │ <pubDate>$(convert_post_date $DATE)</pubDate>\n\
 295   │ <description>$DESC</description>
 296   │ </item>\n" >> "$1/rss.tmp"
 297   │   done
 298   │
 299   │   # Insert into template
 300   │   # 1. HTML
 301   │   [[ ! -f "$1/$2" ]] && echo "HTML template file not found." && return 1
 302   │   tmpf="$1/$2.tmp"
 303   │   [[ -f "$tmpf" ]] && rm "$tmpf"
 304   │   cp "$1/$2" "$tmpf"
 305   │   sed -i "/$3/r $1/index.tmp" "$tmpf"
 306   │   sed -i "/$3/d" "$tmpf"
 307   │   # 2. RSS
 308   │   [[ ! -f "$1/$6" ]] && echo "RSS template file not found." && return 1
 309   │   tmpr="$1/$6.tmp"
 310   │   [[ -f "$tmpr" ]] && rm "$tmpr"
 311   │   cp "$1/$6" "$tmpr"
 312   │   sed -i "/$3/r $1/rss.tmp" "$tmpr"
 313   │   sed -i "/$3/d" "$tmpr"
 314   │
 315   │   #Copy to top level & delete temps
 316   │   # 1. HTML
 317   │   [[ -f "$1/$4" ]] && rm "$1/$4"
 318   │   cp "$tmpf" "$1/$4"
 319   │   rm "$tmpf"
 320   │   rm "$1/index.tmp"
 321   │   # 2. RSS
 322   │   [[ -f "$1/$7" ]] && rm "$1/$7"
 323   │   cp "$tmpr" "$1/$7"
 324   │   rm "$tmpr"
 325   │   rm "$1/rss.tmp"
 326   │ }
``` 
*I DID say it was hacky, right?*

My customized version of SSG5 actually lives right in the structure of this website.
This:
  A) Helps me not forget where I stored it
  B) Gives people the ability to easily understand the way these pages were generated.

If you are interested in comparing my additions to the original, feel free to check it out [here](https://ezofuzz.me/chp/_custom_ssg5).

# Whats to come.

Quite a bit! Depends on what I am doing. During my bachelor semester I will be most likely showcasing some C++ code in relation to real time rendering and game egine development. Occasionally some linux / unix stuff will certainly find its way onto this section as well. While I try to focus on technical concepts and programming, I tend to be a generalist of sorts. Graphics Desing and Mapping for various old school and contemporary shooters are some of my passtimes, which you will get to see down the line as well.
